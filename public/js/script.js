function checkAndRedirect() {
    // Get the value entered by the user
    var enteredNumber = document.getElementById('numberInput').value;
  
    // Check if the entered number is the specific number you want
    if (enteredNumber === '69') {
      // Redirect to another HTML file
      window.location.href = 'games.html'; // Replace with the relative path to your HTML file
    } else {
      alert('stop guessing.'); // Show an alert for an invalid number
    }
  }